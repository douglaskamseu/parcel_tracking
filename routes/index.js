let express = require('express');
let router = express.Router();

const connection = require('../config/database');

connection.connect((err) => {
  if (err) throw err;
  console.log('Connected to Database.')
});

/* GET home page. */
router.get('/', function(req, res, next) {

  //output  the result
  res.setHeader('Content-Type', 'text/html'); //output content is HTML
  res.render('menu', { title: 'Express' });
});

router.get('/customers', function(req, res, next) {

  let sql = 'SELECT DISTINCT * FROM Customers C';

  // response contains a json array with all tuples
  let postProcessSQL =  (err, customers, fields) =>  {
    if (err) throw err;

    //output  the result
    res.setHeader('Content-Type', 'text/html'); //output content is HTML
    res.render('customers', { customers });
  };

  connection.query(sql, postProcessSQL);
});

router.get('/displayParcels', function(req, res, next) {

  const custId = Number(req.query.customer);

  let sql = `SELECT DISTINCT * FROM Parcels P, Locations L WHERE P.custId = ${custId}  AND L.locId = P.finalLocation`;
  let sqlCust = `SELECT DISTINCT * FROM Customers WHERE custId = ${custId}`;

  // response contains a json array with all tuples
  let postProcessSQL =  (err, parcels, fields) => {
    if (err) throw err;

    let postProcessSQLCust = (err, customer, fields) => {
      if (err) throw err;

      //output  the result
      res.setHeader('Content-Type', 'text/html'); //output content is HTML
      res.render('displayParcels', {parcels, customer});
    };

    connection.query(sqlCust, postProcessSQLCust);

  };
    connection.query(sql, postProcessSQL);

});

//creating a new parcel
router.get('/createParcel', function(req, res, next) {

  let sqlCust = 'SELECT * FROM Customers';
  let sqlLoc = 'SELECT * FROM Locations';

  // response contains a json array with all tuples
  let postProcessSQLCust = (err, customers, fields) => {
    if (err) throw err;

    let postProcessSQLLoc = (err, locations, fields) => {
      if (err) throw err;

      res.setHeader('Content-Type', 'text/html'); //output content is HTML
      res.render('createParcel', { customers, locations });
    };

    connection.query(sqlLoc, postProcessSQLLoc);
  };

  connection.query(sqlCust, postProcessSQLCust);
});

//submitting the new parcel
router.get('/newParcel', function(req, res, next) {

  const { finalLocation, customer, weight } = req.query;

  const sql = 'INSERT INTO Parcels(weight, custId, finalLocation) VALUES(?, ?, ?)';
  const values = [weight, customer, finalLocation];

  // create web content once insert statement is processed
  const postProcessInsert = function (err, result) {
    if (err)
      throw err;

    res.setHeader('Content-Type', 'text/html');
    res.render('ackInsertParcel', { id: result.insertId });
  };

  connection.query(sql, values, postProcessInsert);
});

router.get('/manageParcels', function(req, res) {
  res.setHeader('Content-Type', 'text/html');
  res.sendFile( __dirname.substring(0, __dirname.length - 7) + '/views' + '/manageParcels.html');
});


router.get('/manageParcels.js', function(req, res) {
  // send the angular app
  res.setHeader('Content-Type', 'application/javascript');
  res.sendFile( __dirname.substring(0, __dirname.length - 7) + '/js' + '/manageParcels.js');
});

router.get('/env.js', function(req, res) {
  // send the angular app
  res.setHeader('Content-Type', 'application/javascript');
  res.sendFile( __dirname.substring(0, __dirname.length - 7) + '/config' + '/env.js');
});

router.get('/getAllLocations', function(req, res) {

  let sql = 'SELECT locId, locAddress, city FROM Locations';


  // response contains a json array with all tuples
  let postProcessSQL = function (err, result) {
    if (err) throw err;

    res.json(result);
  };

  connection.query(sql, postProcessSQL);


});

router.get('/getAllCustomers', function(req, res) {

  let sql = 'SELECT DISTINCT * FROM Customers C';

  // response contains a json array with all tuples
  let postProcessSQL = function (err, result) {
    if (err) throw err;

    res.json(result);
  };

  connection.query(sql, postProcessSQL);


});



router.get('/getAllParcels', function(req, res) {

  let sql = 'SELECT DISTINCT * FROM Customers C, Parcels P, Locations L WHERE C.custId = P.custId AND L.locId = P.finalLocation';

  // response contains a json array with all tuples
  let postProcessSQL = function (err, result) {
    if (err) throw err;

    res.json(result);
  };

  connection.query(sql, postProcessSQL);

});

router.get('/getParcelHist/:parcelId', function(req, res) {

  const { parcelId } = req.params;

  let sql = `SELECT DISTINCT * FROM Located L, Parcels P, Customers C, Locations LO WHERE L.parcelId = ${parcelId} AND
              P.parcelId = L.parcelId AND C.custId = P.custId AND L.locId = LO.locId`;

  // response contains a json array with all tuples
  let postProcessSQL = function (err, result) {
    if (err) throw err;

    res.json(result);
  };

  connection.query(sql, postProcessSQL);

});

router.get('/newOperation', function(req, res) {
  const { parcelId, locId, date, time, operation } = req.query;

  let sql = 'INSERT INTO Located(parcelId, locId, date, time, operation) VALUES(?, ?, ?, ?, ?)';
  let values = [parcelId, locId, date, time, operation];

  // create a json object containing the inserted operation
  let postProcessSQL = function (err, result) {
    if (err) throw err;
    console.log(result);
    res.json({ parcelId, locId, date, time, operation });
  };

  connection.query(sql, values, postProcessSQL);

});

router.get('/deleteOperation', function(req, res) {
  const { parcelId, locId } = req.query;

  let sql = `DELETE FROM Located WHERE parcelId = ${parcelId} AND locId = ${locId}`;

  // create a json object containing the deleted operation's foreign keys
  let postProcessSQL = function (err, result) {
    if (err) throw err;
    console.log(result);
    res.json({ parcelId, locId });
  };

  connection.query(sql, postProcessSQL);

});


module.exports = router;
