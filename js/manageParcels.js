manParApp = angular.module('angManParApp', []);

manParApp.controller('ParcelHistoryController',   function($scope, $http) {

    let URL_ALL_LOCS = `http://localhost:${port}/getAllLocations`;
    let URL_ALL_CUSTS = `http://localhost:${port}/getAllCustomers`;
    let URL_ALL_PARC = `http://localhost:${port}/getAllParcels`;
    let URL_PARC_HIST = `http://localhost:${port}/getParcelHist`;
    let URL_NEW_OP = `http://localhost:${port}/newOperation`;
    let URL_DEL_OP = `http://localhost:${port}/deleteOperation`;

    $scope.locations = [];
    $scope.customers = [];
    $scope.parcels = [];
    $scope.parcelHistories = [];


    $http.get(URL_ALL_LOCS).then(function(response) {

        $scope.locations =  response.data;

    });

    $http.get(URL_ALL_CUSTS).then(function(response) {

        $scope.customers =  response.data;

    });

    $http.get(URL_ALL_PARC).then(function(response) {

        $scope.parcels =  response.data;

    });

    $scope.setParcel = (id) => {
        $scope.id = id;
        $scope.selectedParcel = $scope.parcels.find(parcel => parcel.parcelId == id);
        console.log($scope.parcels);
        $http.get(URL_PARC_HIST + '/' + id)
            .then(function (response) {

                $scope.operationHistories = response.data;

            });

    };

    $scope.newOperation = function() {
        $http.get(URL_NEW_OP + `?parcelId=${$scope.id}&locId=${$scope.locId}&date=${$scope.date.toJSON().slice(0,10)}&time=${$scope.time.toJSON().slice(11,19)}&operation=${$scope.operation}`)
            .then(function (response) {
                $scope.setParcel($scope.id);
            });
    };

    $scope.rollback = function(parcelId, locId) {
        $http.get(URL_DEL_OP + `?parcelId=${parcelId}&locId=${locId}`)
            .then(function (response) {
                $scope.setParcel($scope.id);
            });
    };
});

